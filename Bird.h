#pragma once
#include "raylib.h"

struct Bird {
    Vector2 m_position{};
    Vector2 m_velocity{};
};