#pragma once
constexpr int g_screenWidth = 800;
constexpr int g_screenHeight = 450;

constexpr int g_cRadius = 40;
constexpr int g_sRadius = 8;
constexpr int g_aRadius = 40;

constexpr float g_cStrength = 0.001f;
constexpr float g_sStrength = 0.05f;
constexpr float g_aStrength = 0.05f;
constexpr float g_mStrength = 0.5f;
constexpr float g_oStrength = 0.5f;
constexpr float g_eStrength = 0.1f;

constexpr float g_maxSpeed = 3.0f;
constexpr float g_minSpeed = 2.0f;