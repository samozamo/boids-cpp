#include "raylib.h"
#include <cmath>
#include "Globals.h"

const Vector2 getVectorWrapped(Vector2 v1, Vector2 v2) {
    float dx{ v1.x - v2.x };
    float dy{ v1.y - v2.y };

    const float dxAbs = abs(dx);
    const float dyAbs = abs(dy);

    if (dxAbs > static_cast<float>(g_screenWidth) / 2) {
        const float direction = dx / dxAbs; // 1 or -1
        dx = (g_screenWidth - dxAbs) * -direction;
    }

    if (dyAbs > static_cast<float>(g_screenHeight) / 2) {
        const float direction = dy / dyAbs; // 1 or -1
        dy = (g_screenHeight - dyAbs) * -direction;
    }

    return Vector2{ dx, dy };
};

const float getDistanceWrapped(Vector2 v1, Vector2 v2) {
    float dx = abs(v1.x - v2.x);
    if (dx > g_screenWidth / 2) {
        dx = g_screenWidth - dx;
    }

    float dy = abs(v1.y - v2.y);
    if (dy > g_screenHeight / 2) {
        dy = g_screenHeight - dy;
    }
    return sqrt(pow(dx, 2.0f) + pow(dy, 2.0f));
}