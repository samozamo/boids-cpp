#include <vector>
#include <cmath>
#include <iostream>
#include <numbers>
#include "raylib.h"
#include "raymath.h"
#include "Bird.h"
#include "Globals.h"

using Flock = std::vector<Bird>;

void initFlock(Flock& flock, int size) {
    for (int i{ 0 }; i < size; ++i) {
        flock.push_back(Bird{});
        flock[i].m_position = Vector2{
            static_cast<float>(GetRandomValue(0, g_screenWidth)),
            static_cast<float>(GetRandomValue(0, g_screenHeight))
        };
        flock[i].m_velocity = Vector2{
            4 * (static_cast<float>(GetRandomValue(0, 100)) / 100) - 2,
            4 * (static_cast<float>(GetRandomValue(0, 100)) / 100) - 2
        };
    }
}

Vector2 getEForce(Vector2 boidPos) {
    Vector2 force{ 0, 0 };

    if (boidPos.x < 100) {
        force = Vector2Add(force, Vector2{ g_eStrength, 0 });
    }
    else if ((g_screenWidth - boidPos.x) < 100) {
        force = Vector2Add(force, Vector2{ -g_eStrength, 0 });
    }

    if (boidPos.y < 100) {
        force = Vector2Add(force, Vector2{ 0, g_eStrength });
    }
    else if ((g_screenHeight - boidPos.y) < 100) {
        force = Vector2Add(force, Vector2{ 0, -g_eStrength });
    }

    return force;
};

Vector2 getMForce(const Bird& boid) {
    if (IsCursorOnScreen()) {
        const Vector2 mousePos{ GetMousePosition() };
        const float distance{ Vector2Distance(boid.m_position, mousePos) };

        if (distance <= g_cRadius) {
            Vector2 vector{ Vector2Subtract(mousePos, boid.m_position) };

            float angle{ Vector2LineAngle(boid.m_velocity, vector) };
            float viewableAngle{ static_cast<float>((2 * std::numbers::pi) / 6) };

            if (angle <= viewableAngle && angle >= -viewableAngle) {
                vector = Vector2Normalize(vector);
                return vector;
            }
        }
    }

    return Vector2{ 0, 0 };
}

void updateFlock(Flock& flock, std::vector<Vector2>& obstacles) {
    const int flockSize{ static_cast<int>(flock.size()) };
    for (int i{ 0 }; i < flockSize; ++i) {
        Vector2 sForce{ 0, 0 }; // seperation
        Vector2 cForce{ 0, 0 }; // cohesion
        Vector2 aForce{ 0, 0 }; // alignment
        Vector2 eForce{ 0, 0 }; // edge
        Vector2 mForce{ 0, 0 }; // mouse
        Vector2 oForce{ 0, 0 }; // obstacle
        int neighbours{ 0 };
        int obstacleNeighbours{ 0 };

        for (int j{ 0 }; j < flockSize; ++j) {
            if (i == j) continue;

            const float distance{ Vector2Distance(flock[i].m_position, flock[j].m_position) };

            const Vector2 vectorDistance{ Vector2Subtract(flock[j].m_position, flock[i].m_position) };

            if (distance <= g_cRadius && distance > g_sRadius) {
                cForce = Vector2Add(cForce, flock[j].m_position);
                aForce = Vector2Add(aForce, flock[j].m_velocity);
                ++neighbours;
            }

            if (distance <= g_sRadius) {
                sForce = Vector2Subtract(sForce, vectorDistance);
            }
        }

        for (const Vector2 obstacle : obstacles) {
            const float distance{ Vector2Distance(flock[i].m_position, obstacle) };

            if (distance <= g_cRadius) {
                Vector2 vector{ Vector2Subtract(obstacle, flock[i].m_position) };

                float angle{ Vector2LineAngle(flock[i].m_velocity, vector) };
                float viewableAngle{ static_cast<float>((2 * std::numbers::pi) / 6) };

                if (angle <= viewableAngle && angle >= -viewableAngle) {
                    oForce = Vector2Add(oForce, vector);
                    ++obstacleNeighbours;
                }
            }
        }

        eForce = getEForce(flock[i].m_position);
        mForce = getMForce(flock[i]);

        if (neighbours) {
            aForce = Vector2Scale(aForce, 1.0f / neighbours);
            aForce = Vector2Subtract(aForce, flock[i].m_velocity);

            cForce = Vector2Scale(cForce, 1.0f / neighbours);
            cForce = Vector2Subtract(cForce, flock[i].m_position);
        }

        if (obstacleNeighbours) {
            oForce = Vector2Normalize(oForce);
        }

        aForce = Vector2Scale(aForce, g_aStrength);
        cForce = Vector2Scale(cForce, g_cStrength);
        mForce = Vector2Scale(mForce, g_mStrength);
        oForce = Vector2Scale(oForce, g_oStrength);
        sForce = Vector2Scale(sForce, g_sStrength);

        flock[i].m_velocity = Vector2Add(flock[i].m_velocity, aForce);
        flock[i].m_velocity = Vector2Add(flock[i].m_velocity, cForce);
        flock[i].m_velocity = Vector2Add(flock[i].m_velocity, eForce);
        flock[i].m_velocity = Vector2Add(flock[i].m_velocity, mForce);
        flock[i].m_velocity = Vector2Add(flock[i].m_velocity, oForce);
        flock[i].m_velocity = Vector2Add(flock[i].m_velocity, sForce);

        if (i % 2) {
            flock[i].m_velocity.x = (1 - 0.0004f) * flock[i].m_velocity.x + 0.0004f;
        }
        else {
            flock[i].m_velocity.x = (1 - 0.0004f) * flock[i].m_velocity.x - 0.0004f;
        }

        const float speed{ Vector2Length(flock[i].m_velocity) };

        if (speed > g_maxSpeed) {
            flock[i].m_velocity = Vector2Scale(flock[i].m_velocity, g_maxSpeed / speed);
        }
        else if (speed < g_minSpeed) {
            flock[i].m_velocity = Vector2Scale(flock[i].m_velocity, g_minSpeed / speed);
        }

        flock[i].m_position.x = Clamp(flock[i].m_position.x + flock[i].m_velocity.x, 0, g_screenWidth);
        flock[i].m_position.y = Clamp(flock[i].m_position.y + flock[i].m_velocity.y, 0, g_screenHeight);
    }
}

//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------

    //Bird Test1{ Vector2{ 50, 200 }, Vector2{ 1, 0 } };
    //Bird Test2{ Vector2{ screenWidth - 50, 200 }, Vector2{ -1, 0 } };
    //Flock flock{
    //    Test1, Test2
    //};

    Flock flock{};
    initFlock(flock, 200);

    std::vector<Vector2> obstacles{};

    InitWindow(g_screenWidth, g_screenHeight, "boids");

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        updateFlock(flock, obstacles);
        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
            std::cout << "pressed\n";

            obstacles.push_back(Vector2{ static_cast<float>(GetMouseX()), static_cast<float>(GetMouseY()) });
        }
        
        if (IsKeyPressed(KEY_Z)) {
            obstacles.pop_back();
        }

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(BLACK);

        for (Vector2 obstacle : obstacles) {
            DrawCircleV(obstacle, 2, RED);
        }

        for (const Bird& bird : flock) {
            DrawCircleV(bird.m_position, 1, RAYWHITE);
        }

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}