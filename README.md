# Boids with C++ and raylib WIP

This is a work in progress version of [my javascript boids](https://gitlab.com/samozamo/boids).

## Aim

To use this as a learning experiment to improve my c++ skills.

## Dependencies

- [raylib](https://www.raylib.com/)
